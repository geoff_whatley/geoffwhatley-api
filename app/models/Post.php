<?php

class Post extends \Eloquent {

  protected $table = 'posts';

  protected $fillable = ['title', 'content', 'url', 'user_id'];

  //protected $hidden = array('user_id');

  public function user() {
    return $this->belongsTo('User');
  }

  public function categories() {
    return $this->belongsToMany('Category');
  }

}