<?php

class Category extends \Eloquent {

  protected $table = 'categories';

	protected $fillable = ['name'];

  protected $hidden = ['created_at', 'updated_at'];

  public function posts() {
    return $this->belongsToMany('Post');
  }

}