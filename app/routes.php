<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
   return View::make('frontend');
});

Route::group(array('domain' => 'admin.geoffwhatley.com'), function() {
  Route::get('/', function() {
    return View::make('admin');
  });
});

//Routes for API
Route::group(array('domain' => 'api.geoffwhatley.com'), function() {
  
  // Routing for API authentication
  // http://api.geoffwhatley.com/auth/{ROUTES} 
  Route::group(array('prefix' => 'auth'), function() {
    Route::get('status', 'Tappleby\AuthToken\AuthTokenController@index'); // retrieve our token?
    Route::post('login', 'Tappleby\AuthToken\AuthTokenController@store'); // generate token
    Route::delete('logout', 'Tappleby\AuthToken\AuthTokenController@destroy'); // destroy our token
  });

  // Routing for API
  // http://api.geoffwhatley.com/v1/{ROUTES} 
  Route::group(array('prefix' => 'v1'), function() {
    // Protected resources
    Route::group(array('before' => 'auth.token'), function() {
      Route::resource('posts', 'PostsController',
        array('only' => array('store', 'update', 'destroy')));
        
      Route::resource('categories', 'CategoriesController',
        array('only' => array('store', 'update', 'destroy')));
    });

    // Public resources
    Route::get('posts/bycategory/{id}', 'PostsController@getByCategory');
    Route::resource('posts', 'PostsController',
      array('only' => array('index', 'show')));

    Route::resource('categories', 'CategoriesController',
      array('only' => array('index', 'show')));
  });
});