<?php
 
class UserTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    //DB::table('users')->delete();
 
        User::create(array(
 
            'first_name'    => 'Geoff',
            'last_name'     => 'Whatley',
            'email'         => 'admin@admin.com',
            'username'      => 'admin',
            'password'      => Hash::make('password')
 
        ));
 
    }
 
}