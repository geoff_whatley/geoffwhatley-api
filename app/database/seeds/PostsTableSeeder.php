<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
      $title = $faker->text(10);

      $url = preg_replace('/[^\da-z]/i', '', $title);
      $url = preg_replace("/[\s_]/", "-", $url);
      $url = strtolower($url);

			$post = Post::create([
        'title' => $title,
        'content' => $faker->text(200),
        'user_id' => 1,
        'url' => $url
			]);

      $post->categories()->attach($faker->numberBetween(1,5));
      $post->categories()->attach($faker->numberBetween(1,5));
		}
	}

}