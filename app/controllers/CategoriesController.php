<?php

class CategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /categories
	 *
	 * @return Response
	 */
	public function index()
	{
		try {
			$categories = Category::all();
			return Response::jsendSuccess($categories);
		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /categories
	 *
	 * @return Response
	 */
	public function store()
	{
		try {
			$input = Input::json();
			$name = $input->get('name');

			$category = Category::create([
				'name' => $name
				]);

			return Response::jsendSuccess($category);
		} catch (Exception $e) {
			return Response::jsendError($e);
		}		
	}

	/**
	 * Display the specified resource.
	 * GET /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try {
			$category = Category::find($id);
			return Response::jsendSuccess($category);
		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		try {
			$category =  Category::find($id);

			$category->name = Request::get('name');
			$category->save();

			return Response::jsendSuccess();
		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try {
			Category::destroy($id);
			return Response::jsendSuccess();
		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}

}