<?php

class PostsController extends \BaseController {

  public function Login() {
    $login_data = array(
      'username' => Input::json('username'),
      'password' => Input::json('password')
      );

    if (Auth::attempt(array('username' => $login_data['username'], 'password' => $login_data['password']))) {
      $authToken = AuthToken::create(Auth::user());
      $publicToken = AuthToken::publicToken($authToken);

      $authed_user = array(
        'username' => Auth::user()->username,
        'token' => $publicToken
        );

      return Response::jsendSuccess($authed_user);
    } else {
      return Response::jsendError();
    }
  }

  public function Logout() {
    try {
      Auth::logout();
      return Response::jsendSuccess();
    } catch (Exception $e) {
      return Response::jsendError($e);
    }
  }
}