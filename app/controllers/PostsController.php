<?php

class PostsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /posts
	 *
	 * @return Response
	 */
	public function index()
	{
		try {
			$page = 1;
			if (Input::get('page') != null) {
				$page = Input::get('page');
			}
			$record_count = Post::count();
			$per_page = 5;
			$last_page = ceil($record_count/$per_page);

			$posts = Post::forPage($page,$per_page)->get();
			foreach ($posts as $post) {
				$post['categories'] = $post->categories()->get();
				$post['preview'] = substr($post['content'], 0, 200);
				unset($post['content']);
			};


			$pagination_data = [
				'last_page' => $last_page,
				'current_page' => $page,
				'per_page' => $per_page,
				'total_records' => $record_count
			];

			$response_object = [
				'pagination' => $pagination_data,
				'data' => $posts
			];
			return Response::jsendSuccess($response_object);
		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /posts
	 *
	 * @return Response
	 * Input {"user_id":0,"title":"","content":"","categories":[{ "id":"0" }, { "id":"0" }]}
	 */
	public function store()
	{
		try {
			$input = Input::json();
			$title = $input->get('title');
			$url = preg_replace('/[^\da-z]/i', '', $title);
      $url = preg_replace("/[\s_]/", "-", $url);
      $url = strtolower($url);

			$category_ids = $input->get('categories');

			$post = Post::create([
				'user_id' => $input->get('user_id'),
				'title' => $title,
				'content' => $input->get('content'),
				'url' => $url
				]);

			if (count($category_ids) > 0) {
				$post->categories()->attach($category_ids);
			}

			return Response::jsendSuccess($post['id']);

		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}

	/**
	 * Display the specified resource.
	 * GET /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try {
			$post = Post::find($id);
			$post['categories'] = $categories = $post->categories()->get();

			return Response::jsendSuccess($post);
		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 * Input {"user_id":0,"title":"","content":"","categories":[{ "id":"0" }, { "id":"0" }]}
	 */
	public function update($id)
	{
		try {
			$post =  Post::find($id);
			$input = Input::json();
			
			$title = $input->get('title');
			$url = preg_replace('/[^\da-z]/i', '', $title);
      $url = preg_replace("/[\s_]/", "-", $url);
      $url = strtolower($url);
			
			$post->title = $input->get('title');
			$post->content = $input->get('content');
			$post->url = $url;
			$post->save();

			$post->categories()->sync($input->get('categories'));

			return Response::jsendSuccess($post['id']);
		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /posts/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		try {
			$post = Post::find($id);

			$category_count = $post->categories()->count();
			
			if ($category_count > 0) {
				$categories = $post->categories()->get();
				
				foreach ($categories as $category) {
					$ids[] = $category['id'];
				}

				$post->categories()->detach($ids);
			}
			Post::destroy($id);
			return Response::jsendSuccess();
		} catch (Exception $e) {
			return Response::jsendError($e);
		}
	}	

	public function getByCategory($id) {
    try {
      $data = Category::find($id)->posts;
      return Response::jsendSuccess($data);
    } catch (Exception $e) {
      return Response::jsendError($e);
    }
	}

}